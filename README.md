# PackitUp - Vue.js

### Get Started

1. Clone the project 
2. Run `npm install` in the root folder. 
3. You will need credentials to get the project working. Create a .env file in the root of your project and input the following values:

```
VUE_APP_URL="https://getpackitup.com"

VUE_APP_GOOGLE_MAPS_KEY=***
VUE_APP_GOOGLE_TAG_MANAGER_ID=***

VUE_APP_FIREBASE_PROJECT_ID=***
VUE_APP_FIREBASE_API_KEY=***
VUE_APP_FIREBASE_AUTH_DOMAIN=***
VUE_APP_FIREBASE_DATABASE_URL=***
VUE_APP_FIREBASE_STORAGE_BUCKET=***
VUE_APP_FIREBASE_MESSAGE_SENDER_ID=***
VUE_APP_FIREBASE_APP_ID=***
```

All the Firebase credentials can be generated by following this guide: 
https://firebase.google.com/docs/web/setup (you will only need to follow steps 1 and 2)
You should be able to find all the credentials in the Firebase console after following the steps to setup a project. 

And a guide to get a Google Maps API Key: 
https://developers.google.com/maps/documentation/javascript/get-api-key

I don’t believe you need to put the Google Tag Manager key in, the app will work without it. 

### Development

To run the project locally and enable hot-reload, run `npm run serve` in the root folder. 

### Production

To generate a production build, run `npm run build` in the root folder. 