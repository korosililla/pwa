module.exports = {
	css: {
    	loaderOptions: {
      		sass: {
        		prependData: `
        			@import "@/assets/css/_colors.scss";
       				`
      		}
    	}
  	},
  	publicPath: process.env.NODE_ENV === 'development'
    	? '/pwa/'
    	: '/'
}