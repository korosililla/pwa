export default {
	name: 'general',
	data() {
		return {
			monthsArray: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
		}
	},
	methods: {
		$_isSafari: function() {
			//var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
			//var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

			const userAgent = navigator.userAgent.toLowerCase();
			const safari = userAgent.indexOf('safari') != -1 && userAgent.indexOf('chrome') == -1;
			safari ? console.log("we're on safari") : console.log("we're on some other browser that doesnt support pwa");

			return safari;
		},
		$_pushSupported: function() {
			return 'serviceWorker' in navigator && 'PushManager' in window;
		},
		$_get_from_storage: function(item) {
			var data = localStorage[item];
			if (data != undefined) {
				try {
					return JSON.parse(data);
				} catch(e) {
					return data;
				}
			} else {
				return false;
			}
		},
		$_save_to_storage: function(name, item) {
			if (typeof item === 'string' || item instanceof String) {
				localStorage[name] = item;
			} else {
				localStorage.setItem(name, JSON.stringify(item));
			}
		},
		$_trip_is_today_or_later: function(obj) {

			const dateToday = obj.dateToday;
			//console.log(obj);

			if (obj.trip != undefined) {
			  const tripValue = obj.trip.value;
			  const tripDate = tripValue['thedate'];

			  if (tripDate != undefined && tripDate != "") {
			    const split = tripDate.split(" ");

			    if (this.monthsArray.indexOf(split[1]) >= 0) { // If this is -1, it's not in English
			      var formattedTripDate = new Date(split[2], this.monthsArray.indexOf(split[1]), split[0]);
			      return formattedTripDate >= dateToday;
			    } else {
			      return false;
			    }
			  } else {
			    return false;
			  }
			} else {
			  console.log("tripvalue is undefined ");
			  return false;
			}
		},
		$_calculateAge: function(birthday) { // birthday is a string

	        var split = birthday.split(" ");
	        var day = split[0];
	        var month = split[1];
	        var monthNumber = this.$root.monthsArray.indexOf(month);
	        var year = split[2];
	        
	        if (monthNumber >= 0) { // a non-English spelled month will return -1 
	            var dateBirthday = new Date(year, monthNumber, day);
	            
	            var ageDifMs = Date.now() - dateBirthday.getTime();
	            var ageDate = new Date(ageDifMs); // miliseconds from epoch
	            return Math.abs(ageDate.getUTCFullYear() - 1970);
	        } else {
	            return "";
	        }
	    },
	    $_get_datepicker_date: function(date) {

	    	var split = date.split(" ");
	        var day = split[0];
	        var month = split[1];
	        var monthNumber = this.$root.monthsArray.indexOf(month); 
	        var year = split[2];

	        if (monthNumber >= 0) { // a non-English spelled month will return -1 
	            return new Date(year, monthNumber, day);
	        }

	    },
	    $_get_string_from_date: function(date) { // Call like this: this.$_get_string_from_date(today.toISOString().substring(0, 10));
	    	console.log("to convert: " + date); // 2019-11-09
	    	const split = date.split("-");
	    	const year = split[0];
	    	const monthNumber = parseInt(split[1]);
	    	const monthName = this.$root.monthsArray[monthNumber - 1];
	    	const day = split[2];

	    	const datetoreturn = day + " " + monthName + " " + year;
	    	console.log(datetoreturn);

	    	return datetoreturn;

	    },
	    $_get_string_from_long_date: function(date) {
	    	const dateString = date.toString();
			const dateSplit = dateString.split(" ");
			const month = dateSplit[1]; // Skipping 0, which is e.g. Tue
			const day = dateSplit[2];
			const year = dateSplit[3];

			return day + " " + month + " " + year;
	    }
	}
}