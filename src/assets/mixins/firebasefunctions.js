import * as firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/messaging'
import * as firebaseui from 'firebaseui'

//import loadImage from xx // https://github.com/blueimp/JavaScript-Load-Image
//import exif from xx // https://github.com/exif-js/exif-js 
import EXIF from 'exif-js'

var config = {
    apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
    authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL,
    projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGE_SENDER_ID,
    appId: process.env.VUE_APP_FIREBASE_APP_ID
};
firebase.initializeApp(config);

firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);

export default {
  name: 'firebasefunctions',
  data() {
    return {
      firebaseUIConfig: {
        callbacks: {
          signInSuccessWithAuthResult: this.firebaseSigninSuccessful,
          uiShown: this.firebaseUIShown,
          signInFailure: this.firebaseSigninFailed
        },
        signInFlow: 'popup',
        signInSuccessUrl: window.location.href,
        /*signInOptions: [
          firebase.auth.GoogleAuthProvider.PROVIDER_ID,
          {
            provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
            scopes: [
              'public_profile',
              'email'
            ]
          },
        ]*/
      },
      
      ownProfileListenerStarted: false,

      messagesArray: [],
      convosObj: {}
    }
  },
  computed: {
    database: function() {
      return firebase.database();
    },
    storage: function() {
      return firebase.storage();
    },
    auth: function() {
      return firebase.auth();
    }
  },
  methods: {
    $_auth_check: function() {
      //console.log("$_auth_check " + this.$store.getters.get_authenticated);
      const self = this;

      this.auth.onAuthStateChanged(function(currentUser) {
        console.log("auth state changed ");
        console.log(currentUser);
        if (currentUser) {
        
          // User is signed in.
          self.$root.authenticated = true;
          self.$store.dispatch('set_authenticated', true);
          self.$root.myUID = currentUser.uid;
          console.log("updated own uid: " + self.$root.myUID);

          self.$_startNudgeTimer();

          //self.$root.showDestinationSearchField = true;

          self.$root.showLoader = false;
          self.$_fetchData();
          /*
          self.$_fetchData(currentUser.uid).then(function(response) {
            self.$root.showLoader = false;
          }).catch(function(error) {
            self.$root.showLoader = false;
            console.log("error occured");
          });*/
          
        } else {

          self.$root.showLoader = false;
          
          // No user is signed in.
          self.$root.authenticated = false;
          self.$store.dispatch('set_authenticated', false);
          self.$root.myUID = null;

          //self.$root.showDestinationSearchField = false;

        }
      });
    },
    $_startNudgeTimer: function() {

      if (!this.$store.getters.get_nudge_shown && this.$_pwa_supported() && !this.$_pwa_installed()) { // Only if it's never been shown on this device/browser before and it supports pwa
        // Count down to 30 seconds and then show the nudge
        const self = this;
        setTimeout(function() { 
          self.showNudge = true;
          self.$store.dispatch('set_nudge_shown', true); 
        }, 30000);
      }
    },
    $_pwa_supported: function() {
      // Possibly checks if the browser supports the web app manifest (and thereby the install of pwas)
        var manifestSupported = document.createElement('link').relList.supports('manifest');
        var pwaInstallSupported = 'onbeforeinstallprompt' in window;
        //var pwaInstallSupported2 = 'onappinstalled' in window;

        return manifestSupported && pwaInstallSupported;
    },
    $_pwa_installed: function() {
      return window.matchMedia('(display-mode: standalone)').matches;
    },
    $_setup_login_ui: function() {
      console.log("setting up login: " + firebaseui.auth.AuthUI.getInstance())
      if (firebaseui.auth.AuthUI.getInstance()) {

        const ui = firebaseui.auth.AuthUI.getInstance()
        ui.start('#firebaseui-auth-container', this.firebaseUIConfig)

      } else {

        const ui = new firebaseui.auth.AuthUI(this.auth);
        ui.start('#firebaseui-auth-container', this.firebaseUIConfig);
      }
    },
    firebaseUIShown: function() {
      console.log("uishown");
      document.getElementById('loader').style.display = 'none';
    },
    firebaseSigninFailed: function(error) {
      console.log("sign in failed " + error);
      return true;
    },
    firebaseSigninSuccessful: function(authResult, redirectUrl) {
      console.log("authentication successful");
      /*var user = firebase.auth().currentUser;
      saveUser(user).then((msg) => {
          //console.log(msg);
          location.reload();
      }).catch((error) => {
          console.log(error);
          //location.reload();
      });*/

      return true;
    },
    $_sign_in_with_email: function(email, password) {
      const self = this;
      return new Promise(function(resolve, reject){

        self.auth.signInWithEmailAndPassword(email, password).then(function(user){
          
          resolve(user);
          
        }).catch(function(error) {
          console.log(error.code + ' - ' + error.message);
          //reject(error.message);
          const msg = error.code == "auth/wrong-password" ? "Sorry, that is the wrong password." : "Sorry! An error occured. Please try again later."; 
          alert(msg);
        });

      }).catch(function(error) {
        reject(error);
      });
    },
    $_getTripData: function() { // THIS IS THE ONE WE'RE USING
      const self = this;
      this.$root.showLoader = true;

      const dateToday = new Date();

      return new Promise(function(resolve, reject) {
        self.database.ref('routes').once('value').then(function(snapshot) {
          console.log("trips received");
          resolve(snapshot);
        }).catch(function(error) {
          console.log("ERROR WITH FIREBASE: " + error);
          reject(error);
        });
      });

      // And starting the listeners 
      ref.on('child_changed', function(data) {
        console.log("TRIP CHANGED " + data.val());
      });

      ref.on("child_removed", function(data) { 
        console.log("TRIP REMOVED " + data.key);
        if (data.key != undefined) {
          self.$store.dispatch('remove_trip', data.key);
        }
      })
    },
    $_getData: function() {
      const self = this;
      return new Promise(function(resolve, reject) {
        //self.database.ref('users').limitToLast(1600).endAt(800).once('value').then(function(snapshot) {
        self.database.ref('users').limitToLast(800).once('value').then(function(snapshot) {
          var emails = [];

          snapshot.forEach(function(user) {
            var email = user.val().email;
            emails.push(email);
          });
          resolve(emails);
        });
      });
    },
    /*$_getTripData: function(startPosition) {
      console.log("$_getTripData");
      const self = this;
      return new Promise(function(resolve, reject) {
        self.database.ref('routes').once('value').then(function(snapshot) {
          snapshot.forEach(function(trip) {

            const key = trip.key;
            const value = trip.val();

            self.$store.dispatch('add_trip_to_arrays', {key: key, value: value})
          });
          resolve('success');

        }).catch(function(error) {
          console.log("ERROR WITH FIREBASE: " + error);
          reject(error);
        });
      }); 
    },*/
    $_fetchData: function() {
      var self = this;

      //const messagesLoaded = this.$store.getters.get_message_list_length;
      //this.$_listenForMessages(messagesLoaded);
      
      this.$_listenForMessages();

      //const profilesLoaded = this.$store.getters.get_user_list_length;
      //this.$_listenForProfiles(profilesLoaded);

      //return new Promise(function(resolve, reject) {
        
        /*self.$_startProfileListener().then(function(response) {
          console.log("profiles successfully fetched " + self.$store.getters.get_user_list_length);
          resolve('success');
        }).catch(function(error) {
          console.log("error fetching profiles in trip popup " + error);
          reject(error);
        });*/

        /*self.$_startMessageListener(uid).then(function(response) {
          console.log("messages successfully fetched " + response);
          resolve('success');
        }).catch(function(error) {
          console.log("error fetching messages in trip popup " + error);
          reject(error);
        });*/
      //});
    },
    $_startProfileListener: function() {
      /*console.log("$_startProfileListener - just getting data once");
      const self = this;

      return new Promise(function(resolve, reject) {

        if (self.$root.authenticated) {
          self.database.ref('users').once('value').then(function(snapshot) {
            snapshot.forEach(function(user) {
              const key = user.key;
              const value = user.val();

              if (self.$store.getters.get_number_of_users > 0) { // Only adding to our array if not already in there
                const profileIndex = self.$store.getters.get_userindex_by_key(key);
                if (profileIndex === -1){
                  self.$store.dispatch('add_profiles', {key: key, value: value});
                }
              } else {
                self.$store.dispatch('add_profiles', {key: key, value: value});
              }
            });
            resolve('success');
          }).catch(function(error) {
            console.log("ERROR WITH FIREBASE: " + error);
            reject(error);
          });
        } else {
          reject('not authorized ' + self.$root.authenticated);
        }
      });*/



      /*
      console.log("$_startProfileListener");
      this.database.ref('users').on('child_added', function(snapshot) {
        const key = snapshot.key;
        const value = snapshot.val()
        this.$store.dispatch('add_profiles', {key: key, value: value})
      });
      */
    },
    /*$_startMessageListener: function(uid) {
      console.log("$_startMessageListener " + uid);
      const self = this;

      return new Promise(function(resolve, reject) {

        if (self.$root.authenticated && uid != undefined) {
          if (self.$store.getters.get_message_list_length == 0) {
            self.database.ref(uid + 'messages').once('value').then(function(snapshot) {
              console.log("saving messages with uid: " + uid);
              snapshot.forEach(function(message) {
                const key = message.key;
                const value = message.val();

                self.$store.dispatch('add_messages', {key: key, value: value});
                self.$store.dispatch('add_convos', {key: key, value: value, myUID: uid});
              });
              resolve('success');
            }).catch(function(error) {
              console.log("ERROR WITH FIREBSE: " + error);
              reject(error);
            });
          } else {
            resolve('success - list has already been fetched ' + self.$store.getters.get_message_list_length);
          }
        } else {
          reject('not authenticated - root.authenticated:' + self.$root.authenticated);
        }
      });
    },*/
    $_listenForTrips: function(startPosition) { // This can ONLY be used in the map (where the correct callback exists)
      //console.log("$_listenForTripChanges");
      /*const self = this;
      const ref = this.database.ref('routes');

      ref.on('child_added', function(data) {
        //console.log("trip added " + data);

        const key = data.key;
        const value = data.val();

        self.addMarker({ key: key, value: value });

        if (startPosition > 0) { // Only adding to our array if not already in there
          const tripIndex = self.$store.getters.get_tripindex_by_key(key);
          if (tripIndex === -1){
              self.$store.dispatch('add_trip_to_arrays', {key: key, value: value});
          }
        } else {
          self.$store.dispatch('add_trip_to_arrays', {key: key, value: value});
        }

      });

      ref.on('child_changed', function(data) {
        console.log("TRIP CHANGED " + data.val());
      });

      ref.on("child_removed", function(data) { 
        console.log("TRIP REMOVED " + data.key);
        if (data.key != undefined) {
          self.$store.dispatch('remove_trip', data.key);
        }
      })*/
    },
    $_listenForMessages: function() {
      console.log("starting to listen for messages");
      
      if (this.$root.authenticated && this.$root.myUID != undefined) {
        const self = this;
        const uid = this.$root.myUID;
        const ref = this.database.ref(uid + 'messages');


        ref.on('child_added', function(data) {

          const key = data.key;
          const value = data.val();

          const msgToAdd = {key: key, value: value};

          // Message array 
          self.messagesArray.push(msgToAdd);

          // Convos object
          const convokey = value.senderID == uid ? value.recipientID : value.senderID;
          var unread;

          if (self.convosObj[convokey] != undefined) {
            unread = self.convosObj[convokey].unread++;
          } else {
            unread = value.unread == "unread" ? 1 : 0;
          }

          if (convokey != undefined) { self.convosObj[convokey] = { value: value, unread: unread }; }

        });

        ref.once('value').then(function(snapshot) { // This is guaranteed to run every time the child listener finishes 

          console.log(self.convosObj);
          //console.log(self.messagesArray);

          self.$store.dispatch('add_messages', self.messagesArray);
          self.$store.dispatch('add_convos', self.convosObj);

          /*const key = data.key;
          const value = data.val();

          if (startPosition > 0) { // Only adding to our array if not already in there
            const messageIndex = self.$store.getters.get_messageindex_by_key(key);
            if (messageIndex === -1) {
                self.$store.dispatch('add_messages', {key: key, value: value});
                self.$store.dispatch('add_convos', {key: key, value: value, myUID: uid});
            }
          } else {
            self.$store.dispatch('add_messages', {key: key, value: value});
            self.$store.dispatch('add_convos', {key: key, value: value, myUID: uid});
          }*/

        });
      }
/*
        ref.on('child_changed', function(data) {
          console.log("MESSAGE CHANGED " + data.val());
        });

        ref.on("child_removed", function(data) { 
          console.log("TRIP REMOVED " + data.key);
          if (data.key != undefined) {
            self.$store.dispatch('remove_trip', data.key);
          }
        })
*/
    },
    /*$_listenForProfiles: function(startPosition) {
      if (this.$root.authenticated && this.$root.myUID != undefined) {
        const self = this;
        const ref = this.database.ref('users');

        const uid = this.$root.myUID;
        const myRef = this.database.ref('users/' + uid);

        ref.on('child_added', function(data) {

          const key = data.key;
          const value = data.val();

          if (startPosition > 0) { // Only adding to our array if not already in there
            const profileIndex = self.$store.getters.get_userindex_by_key(key);
            if (profileIndex === -1) {
                self.$store.dispatch('add_profiles', {key: key, value: value});
            }
          } else {
            self.$store.dispatch('add_profiles', {key: key, value: value});
          }

        });

        myRef.on('child_changed', function(data) {
          console.log("CHANGED " + data.key);
          data.uid = uid;
          self.$store.dispatch('update_profile_info', data);
        });
      }
    },*/
    $_listenForProfileChanges: function(uid) {
      const self = this;

      const userRef = this.database.ref('users/' + uid);

      userRef.on('child_changed', function(data) {
        console.log("CHANGED " + data.key);
        data.uid = uid;
        self.$store.dispatch('update_profile_info', data);
      });
    },
    $_downloadFirebaseData: function(reference) {
      const self = this;
      return new Promise(function(resolve, reject) {

        self.database.ref(reference).once('value').then(function(snapshot) {
          if (snapshot.exists()) {
            resolve({ key: snapshot.key, value: snapshot.val() });
          } else {
            reject("Doesn't exist");
          }
        }).catch(function(error) {
          console.log("ERROR WITH FIREBSE: " + error);
          reject(error);
        });
      });
    },
    $_downloadFirebasePhoto: function(pictureURL) { // Downloading a photo from our Firebase Storage Bucket - used in Map and Profile

      // Creating a reference to the storage bucket
      var storage = firebase.storage();
      var storageRef = storage.refFromURL(pictureURL)

      return new Promise(function(resolve, reject) {

          storageRef.getDownloadURL().then(function(url) {

              window.loadImage(url, function (img) {
                if (img.type === "error") {
                  //console.log("couldn't load image:", img);
                } else {
                  EXIF.getData(img, function () {

                      var orientation = EXIF.getTag(this, "Orientation");
                      var canvas = window.loadImage.scale(img, {orientation: orientation || 0, canvas: true, maxWidth: 600, maxHeight: 600, crop: true, cover:true});
                      //var canvas = window.loadImage.scale(img, {orientation: orientation || 0, canvas: true});

                      resolve(canvas);

                  });
                }
              }, {cover: true});

          }).catch(function(error) {
            // Handle any errors
            //console.log("error getting picture " + error);
            reject("Error getting picture " + error);
          });

      });
    },
    $_loadMyImage: function(imageURL, div) {
      console.log("loading image " + div);
      div.innerHTML = ""; // Making sure there are no old canvases in there already 

      // Main.js - trip profile pic
      if (imageURL.indexOf("gs://shining-fire-5731.appspot.com") >= 0) { // It's a Firebase photo
          this.$_downloadFirebasePhoto(imageURL).then(function(imagecanvas) {
              if (imagecanvas) {

                // Adding the css id to the img canvas and adding it to the layout
                //imagecanvas.id = picID;
                div.appendChild(imagecanvas);
              }
          });
          
      } else {

          var imgElement = document.createElement('img');
          //imgElement.id = picID;
          imgElement.src = imageURL;
          imgElement.onerror = function() { imgElement.src = '/images/anonymous-profile-pic.jpg' };

          div.appendChild(imgElement);
          
      }
    },
    $_signOut: function() {
      const self = this;
      this.auth.signOut().then(function() {
          self.$root.myUID = undefined;
          self.$root.authenticated = false;

          // Clear storage
          self.$store.dispatch('reset_state');
          // location.reload();
          self.$router.push('/');
          
        }, function(error) {
          console.error('Sign out error', error);
        });
        
        return false; 
    },
    $_updateFirebaseData: function(ref, updates) {

      // Updating the speficied fields - this requires e.g. 'users/uid' as the ref and then e.g. '{"profileImage": url}' as the update
      //var updateref = this.database.ref(ref);
      
      /*var updateref = this.database.ref(ref);
      console.log(ref);
      console.log(updateref);
      console.log(updates); */
      this.database.ref(ref).update(updates);

    },
    $_setFirebaseData: function(ref, data) {
      this.database.ref(ref).set(data);
    },
    $_pushFirebaseData: function(ref, data) {
      //console.log("pushing:");
      //console.log(ref);
      //console.log(data);
      this.database.ref(ref).push(data);
    },
    $_setFirebaseDataWithPromise: function(ref, data) {

      const self = this;

      return new Promise(function(resolve, reject) {

        self.database.ref(ref).set(data).then(function() {
          resolve("success");
        }).catch(function(error) {
          reject(Error("There was an error saving your profile. Please try again."));
        });
        
      });
    },
    $_removeFirebaseData: function(ref) {
      // Removing the specified reference from Firebase
      this.database.ref(ref).remove();
    },
    $_get_firebase_item(object, selector) { // To avoid undefined
      if (object != undefined && object[selector] != undefined && object[selector] != null) {
        return object[selector];
      } else {
        return "";
      }
    },
    $_changePassword: function(oldpassword, newpassword) {
      const self = this;
      return new Promise(function(resolve, reject) {
        var user = self.auth.currentUser;
        const email = user.email;
        var credential = firebase.auth.EmailAuthProvider.credential(email, oldpassword);
        user.reauthenticateWithCredential(credential).then(function() {
            // User re-authenticated.

            user.updatePassword(newpassword).then(() => {
                // Update successful.
                $(settingsChangePasswordWarning0).hide();
                $(settingsChangePasswordWarning1).hide();
                $(settingsChangePasswordWarning2).hide();
                $(settingsChangePasswordSuccess).show();

                // Go back to settingslist
                $(settingsChangePasswordDiv).delay(1000).fadeOut("slow", function() {
                    $(settingsList).show('slide', {direction: 'right'}, "slow");
                    $(settingsModal).find(".modal-footer").hide();
                });

            }, (error) => {
                // An error happened.
                $(settingsChangePasswordWarning0).hide();
                $(settingsChangePasswordWarning1).hide();

                $(settingsChangePasswordWarning2).text(error.message);
                $(settingsChangePasswordWarning2).show();
            });

        }).catch(function(error) {
            // An error happened.
            console.log('there was an error signin in ' + error.message);
            alert("Sorry! An error occured while changing your password.");
        });
      }).catch(function(error) {
        console.log("Error changing password " + error);
      });
    },
    $_saveImage: function(newpicFile) {
      const self = this;
      return new Promise(function(resolve, reject) {
        // Create a Firebase storage ref
        var storageRef = self.storage.ref();
        var imageRef = storageRef.child('profileimages/' + self.$root.myUID);
        var metadata = { contentType: 'image/jpeg' };

        imageRef.put(newpicFile, metadata).then(function(snapshot) {
          var imageurl = imageRef.toString();
          resolve(imageurl);

          //updates.profileImage = imageurl;
          //imageUpdate.profileImage = imageurl;

          //updateFirebaseData(updateref, imageUpdate); // This needs to be updated on it's own, since it's not done when the rest is being saved 

          //showProfileImage(imageurl);
        });
      }).catch(function(error) {
        reject(error);
      });
    },
    $_createUser: function(userObj, password) {
      const self = this;
      this.auth.createUserWithEmailAndPassword(userObj.email, password).then(function(user){

          const thisUser = self.auth.currentUser;
          self.$root.myUID = thisUser.uid;
          const ref = '/users/' + thisUser.uid;
          
          //saveFirebaseInfo(signupFirstname, signupLastname, signupEmail, signupBirthday, signupNationality, signupGender, signupPlatform, signupUID);
          self.$_setFirebaseData(ref, userObj);

          // We should be fetching the new profile when we get authenticated, but to be safe
          self.$store.dispatch('add_profiles', {key: thisUser.uid, value: userObj});

          // And dismissing the modal - automatically signs in
          //$(signInModal).modal('toggle');
          self.$root.showLoginModal = false;
          self.$root.showLoginModalKey++;
          
      }).catch(function(error) {
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorCode + ' - ' + errorMessage);
          
          if (errorCode === "auth/email-already-in-use") {
              alert("That email has already signed up. Please sign in.");
          }
      });
    },
    $_deleteUser: function() {
      if (confirm('Are you sure you want to delete your user? This cannot be undone.')) {
        // Delete it
        if (this.$root.myUID != null && this.$root.myUID != undefined) {
            //firebase.database().ref('routes/' + tripUID).remove();
            var ref = 'users/' + this.$root.myUID;
            const self = this;
            //removeFirebaseData(ref);
            firebase.database().ref(ref).remove().then(function() {

                this.$root.authenticated = false;
                this.$root.myUID = "";

                // THIS PERMANENTLY DELETES THE USER FROM FIREBASE AUTH
                /*var user = firebase.auth().currentUser;
                user.delete().then(function() {
                  // User deleted.
                  signedin = false;
                  // Now, update the UI
                  location.reload();
                }).catch(function(error) {
                  // An error happened.
                  alert("Sorry, there was a problem deleting your user. Please contact the PackitUp team.");
                  console.error('Delete user error', error);
                });*/

                self.signOut();

                /*self.auth.signOut().then(function() {
                    self.$root.authenticated = false;
                    self.$root.myUID = "";

                    // Now, update the UI
                    self.$router.push('/');
                    //location.reload(); 

                }, function(error) {
                    alert("Sorry, there was a problem deleting your user. Please contact the PackitUp team.");
                    console.error('Sign out error', error);
                });*/
            });
        } else { 
            alert("Sorry, there was a problem deleting your user. Please contact the PackitUp team.");
        }
      }
    },
    async askForPermissionToReceiveNotifications() {
      const self = this;
      try {
        const messaging = firebase.messaging();
        await messaging.requestPermission();
        const token = await messaging.getToken();

        if (self.$root.myUID != undefined && self.$root.myUID != null) {
            var ref = "users/" + self.$root.myUID + "/token";
            self.$_setFirebaseDataWithPromise(ref, token).then((msg) => {
                console.log("user token saved");
            }).catch((error) => {
                console.log("user token error " + error);
            });
        }
        
        return token;
      } catch (error) {
        console.error(error);
      }
    },
    listenForTokenRefresh: function() { // Callback fired if Instance ID token is updated.
      const messaging = firebase.messaging();
      const self = this;

      messaging.onTokenRefresh(() => {
        messaging.getToken().then((refreshedToken) => {
          console.log('Token refreshed.');
          if (self.$root.myUID != undefined && self.$root.myUID != null) {
            var ref = "users/" + self.$root.myUID + "/token";
            self.$_setFirebaseDataWithPromise(ref, refreshedToken).then((msg) => {
                console.log("refreshed user token saved");
            }).catch((error) => {
                console.log("user token error " + error);
            });
          }
        }).catch((err) => {
          console.log('Unable to retrieve refreshed token ', err);
          showToken('Unable to retrieve refreshed token ', err);
        });
      });
    }
  }
}
