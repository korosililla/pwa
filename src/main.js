import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

import VCalendar from 'v-calendar'
Vue.use(VCalendar)

import { VueHammer } from 'vue2-hammer'
Vue.use(VueHammer)

Vue.config.productionTip = false

import '@/assets/css/legacy-styling.css'
import "@/assets/css/_global.scss" // Importing global styling

import firebasefunctions from "@/assets/mixins/firebasefunctions.js"

new Vue({
	mixins: [firebasefunctions],
	data() {
		return {
			authenticated: false,
			myUID: undefined,
			mobile: false,
			showLoader: true,

			activeNav: "Home",
			
			showLoginModal: false,
			showLoginModalKey: 100,

			showNewTripModal: false,
			showNewTripModalKey: 1000,
			
			showDestinationSearchField: true,

			monthsArray: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],

			tripsArray: [], // All trips
			profilelist: [] // All profiles
		}
	},
	created() {
		this.$_auth_check();
	},
	store,
	router,
	render: h => h(App)
}).$mount('#app')
