import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Messages from './views/Messages.vue'
import Profile from './views/Profile.vue'

//component: () => import('./views/Messages.vue')
//component: () => import('./views/Profile.vue')

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/messages',
      name: 'Messages',
      component: Messages
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    }
  ]
})
