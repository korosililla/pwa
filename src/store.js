import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const monthsArray = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

const vuexPersist = new VuexPersist({
  key: 'PackitUp',
  storage: window.localStorage
})

const getDefaultState = () => {
  return {
    authenticated: false,
    PWANudgeShown: false,
    tripsArray: [], // All trips - format: {key: key, value: {}}
    loadedTripIDS: [],
    activeTripsArray: [], // All trips happening today or after
    //profilelist: [], // All profiles
    convolist: {}, // All conversations
    messagelist: [], // All messages
    unreadmessages: 0,
    pushNudgeShown: false
  }
}

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  state: getDefaultState(),
  mutations: { // Called with $store.commit
    RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers - https://github.com/vuejs/vuex/issues/1118
      Object.assign(state, getDefaultState());
    },
    SET_AUTHENTICATED(state, boolean) {
      state.authenticated = boolean;
    },
    SET_PWA_NUDGE_SHOWN(state, boolean) {
      state.PWANudgeShown = boolean;
    },
    SET_PUSH_NUDGE_SHOWN(state, boolean) {
      state.pushNudgeShown = boolean;
    },
    RESET_TRIPS(state) {
      //state.tripsArray.length = 0;
      //state.activeTripsArray.length = 0;
    },
  	ADD_TRIP(state, trip) {
  		//state.tripsArray.push(trip);
      console.log("adding array to storage in $store");

      Array.prototype.push.apply(state.tripsArray, trip);

      //const existingLength = state.tripsArray.length;
      //state.tripsArray.splice(0, existingLength, state.tripsArray.concat(trip));
  	  
      //state.tripsArray.concat(trip);
    },
    ADD_LOADED_TRIP(state, trip) {
      //state.loadedTripIDS.push(trip);
      //state.loadedTripIDS.concat(trip);

      Array.prototype.push.apply(state.loadedTripIDS, trip);
      
      //const existingLength = state.loadedTripIDS.length;
      //state.loadedTripIDS.splice(0, existingLength, state.loadedTripIDS.concat(trip));
    },
  	ADD_ACTIVE_TRIP(state, trip) {
      //state.activeTripsArray.push(trip);
  		//state.activeTripsArray.concat(trip);

      Array.prototype.push.apply(state.activeTripsArray, trip);
      
      //const existingLength = state.activeTripsArray.length;
      //state.activeTripsArray.splice(0, existingLength, state.activeTripsArray.concat(trip));

    },
  	UPDATE_TRIP(state, updatedtrip) {
  		state.tripsArray.forEach(function(item, index) {
  			if (item.key === updatedtrip.key) {
  				state.tripsArray[index] = updatedtrip;
  			}
  		})
  	},
    REMOVE_TRIP(state, tripID) {
      const tripIndex = state.tripsArray.findIndex(x => x.key == tripID);
      if (tripIndex > -1) {
        state.tripsArray.splice(tripIndex, 1);

        const activeTripIndex = state.activeTripsArray.findIndex(x => x.key == tripID);
        if (activeTripIndex > -1) {
          state.activeTripsArray.splice(activeTripIndex, 1);
          console.log("removed " + tripID);
        }
      } else {
        console.log("Trip not found in storage");
      }
    },
  	/*ADD_USERS(state, profiles) {
  		state.profilelist.push(profiles);
  	},*/
    UPDATE_PROFILE_INFO(state, update) {
      var foundIndex = state.profilelist.findIndex(x => x.key == update.uid);
      state.profilelist[foundIndex].value[update.key] = update.val();
    },
    ADD_CONVO(state, message) {
      
      Object.assign(state.convolist, message);
      /*const key = message.value.senderID == message.myUID ? message.value.recipientID : message.value.senderID;
      const value = message.value;

      var unread;
      if (state.convolist[key] != undefined) {
        var currentunreadcount = state.convolist[key].unread;
        unread = currentunreadcount++;
      } else {
        unread = 1;
      }

      if (key != undefined) {
        state.convolist[key] = { value: value, unread: unread }; // This replaces the convo everytime another message is looped through, so we'll have the latest ones
      }*/
    },
    RESET_UNREAD_COUNT(state, uid) {
      state.convolist[uid].unread = 0;
    },
  	ADD_MESSAGES(state, messages) {
  		//state.messagelist.push(messages)
  	
      Array.prototype.push.apply(state.messagelist, messages);

    }
  },
  getters: { // Called with $store.getters
    get_authenticated: state => {
      return state.authenticated;
    },
    get_nudge_shown: state => {
      return state.PWANudgeShown;
    },
    get_push_nudge_shown: state => {
      return state.pushNudgeShown;
    },
    get_number_of_trips: state => {
  		return state.tripsArray != undefined ? state.tripsArray.length : 0;
  	},
  	get_all_trips: state => {
  		return state.tripsArray;
  	},
  	get_all_active_trips: state => {
  		return state.activeTripsArray;
  	},
  	get_trips_for_UID: (state) => (uid) => {
  		return state.tripsArray.filter(trip => trip.value.sender === uid);
  	},
    get_tripkey_by_index: (state) => (index) => {
      return state.tripsArray[index].key;
    },
    get_tripindex_by_key: (state) => (tripID) => {
      return state.tripsArray.findIndex(x => x.key === tripID);
    },
  	get_trip: (state) => (tripID) => {
		  return state.tripsArray.find(o => o.key === tripID);
  	},
    get_active_trip_index: (state) => (tripID) => {
      return state.activeTripsArray.findIndex(o => o.key === tripID);
    },
    get_trip_loaded: (state) => (tripID) => {
      return state.loadedTripIDS.indexOf(tripID) > -1;
    },

  	/*get_number_of_users: (state) => {
      return state.profilelist.length;
    },
    get_all_users: (state) => {
      return state.profilelist;
    },
    get_user: (state) => (uid) => {
  		return state.profilelist.filter(user => user.key == uid);
  	},
    get_userindex_by_key: (state) => (UID) => {
      return state.profilelist.findIndex(x => x.key === UID);
    },
    find_user: (state) => (uid) => {
      return state.profilelist.find(user => user.key == uid);
    },
    get_user_list_length: (state) => {
      return state.profilelist.length;
    },*/

    get_convos: (state) => {
      return state.convolist;
    },
  	get_messages: (state) => {
  		return state.messagelist;
  	},
    get_messageindex_by_key: (state) => (messageID) => {
      return state.messagelist.findIndex(x => x.key === messageID);
    },
    get_message_list_length: (state) => {
      return state.messagelist.length;
    },
    get_messages_for_UID: (state) => (uid) => {
      return state.messagelist.filter(message => message.value.senderID == uid || message.value.recipientID == uid);
    },
    get_unread_count: (state) => {
      var allunread = 0;
      for (var i = 0; i < state.convolist.length; i++) {
        const convounread = state.convolist[i].unread;
        allunread = allunread + convounread;
      }

      return allunread;
    },
    get_unread_count_for_convo: (state) => (uid) => {
      return state.convolist[uid].unread;
    }
  },
  actions: { // Called with $store.dispatch
    reset_state(context) {
      context.commit('RESET_STATE');
    },
    set_authenticated(context, boolean) {
      context.commit('SET_AUTHENTICATED', boolean);
    },
    set_nudge_shown(context, boolean) {
      context.commit('SET_PWA_NUDGE_SHOWN', boolean);
    },
    set_push_nudge_shown(context, boolean) {
      context.commit('SET_PUSH_NUDGE_SHOWN', boolean);
    },
    reset_trips(context) {
      context.commit('RESET_TRIPS');
    },
  	add_trip_to_array(context, trip) {

      context.commit('ADD_TRIP', trip);

      // Sorting the future trips to add to the relevant array
      /*var dateToday = new Date();
      var tripDate = trip.value['thedate'];

      if (tripDate != undefined && tripDate != "") { // Formatting the date
        const split = tripDate.split(" ");

        if (monthsArray.indexOf(split[1]) >= 0) {
          var formattedTripDate = new Date(split[2], monthsArray.indexOf(split[1]), split[0]);
          if (formattedTripDate >= dateToday) {
              context.commit('ADD_ACTIVE_TRIP', trip);
          }
        }
      }*/
  	},
    add_loaded_trip_to_array(context, trip) {
      context.commit('ADD_LOADED_TRIP', trip);
    },
    add_trip_to_active_array(context, trip) {
      context.commit('ADD_ACTIVE_TRIP', trip);
    },
  	update_trip_info(context, updatedtrip) {
  		context.commit('UPDATE_TRIP', updatedtrip);
  	},
    remove_trip(context, tripID) {
      console.log("REMOVING TRIP " + tripID);;
      context.commit('REMOVE_TRIP', tripID);
    },
  	/*add_profiles(context, profiles) {
  		context.commit('ADD_USERS', profiles)
  	},*/
    update_profile_info(context, update) {
      context.commit('UPDATE_PROFILE_INFO', update);
    },
    add_convos(context, message) {
      context.commit('ADD_CONVO', message);
    },
    reset_unread_count(context, uid) {
      context.commit('RESET_UNREAD_COUNT', uid);
    },
  	add_messages(context, messages) {
  		context.commit('ADD_MESSAGES', messages)
  	}
  }
})